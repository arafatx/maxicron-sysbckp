#/bin/sh - Directadmin custombuild update checker

function sysbckp_clean()
{

#GLOBAL VARS
USER_NAME=admin #directadmin username. which user should view this backup file in file manager. example admin
BACKUP_TEMP_PATH=/backup/da/temp
BACKUP_NEW_PATH=/home/$USER_NAME/system_backups
SYSBCKP_LOG_PATH="/usr/local/bin/maxicron/sysbckp/log"
REPORT_FILE="$SYSBCKP_LOG_PATH/sysbckp-cron.log"
WARNING_STATUS="OK"
MYEMAIL="webmaster@sofibox.com"
MYHOSTNAME=`/bin/hostname`
MAIL_BIN="/usr/local/bin/mail"

echo "================================================================" | tee $REPORT_FILE
echo "[sysbckp-clean] System Backup Script Cleaning checked on `date`" | tee -a $REPORT_FILE
echo "[sysbckp-clean] System Backup Script is cleaning packages and temporary files.." | tee -a $REPORT_FILE
echo "[sysbckp-clean] Please wait..." | tee -a $REPORT_FILE
echo "" | tee -a $REPORT_FILE

#/usr/local/directadmin/custombuild/./build update | tee -a $REPORT_FILE
/usr/local/directadmin/custombuild/./build clean | tee -a $REPORT_FILE
/usr/local/directadmin/custombuild/./build clean_old_webapps | tee -a  $REPORT_FILE
/usr/local/directadmin/custombuild/./build clean_old_tarballs | tee -a $REPORT_FILE
# Manual
rm -f /usr/local/directadmin/custombuild/*.tgz
rm -f /usr/local/directadmin/custombuild/*.tar.gz
rm -f /usr/local/directadmin/custombuild/*.tar.bz2
rm -rf /usr/local/directadmin/custombuild/mysql
rm -rf /usr/local/directadmin/scripts/packages/*
rm -rf /usr/local/directadmin/custombuild/mysql_backups/*
echo "[sysbckp-clean] Done cleaning packages and temporay files" | tee -a $REPORT_FILE
#echo "================================================================" | tee -a  $REPORT_FILE
#echo "[sysbckp-clean] Check status: $WARNING_STATUS"
#mail -s "[sysbckp-clean][$WARNING_STATUS|N] System Backup Script Cleaning Report $MYHOSTNAME"  $MYEMAIL < $FILE_REPORT
#$MAIL_BIN -s "[sysbckp-clean][$WARNING_STATUS|N] System Backup Script Cleaning Report @ $MYHOSTNAME" $MYEMAIL < $FILE_REPORT
#echo "[sysbckp-clean] Done checking system. Email is set to be sent to $MYEMAIL"
#echo "================================================================"
#rm -rf $FILE_REPORT Jangan buang sebab postbk() nak pakai

# Move current backup to temporary folder if the backup exists
if [ "$(ls -A $BACKUP_NEW_PATH)" ]; then
   echo "[sysbckp-script][notice] There are one or more backup exist in $BACKUP_NEW_PATH" | tee -a $REPORT_FILE
   echo "[sysbckp-script][notice] Moving backup files into temporary folder in $BACKUP_TEMP_PATH" | tee -a $REPORT_FILE
   mv "${BACKUP_NEW_PATH}"/* "${BACKUP_TEMP_PATH}"/
   echo "[sysbckp-script][done] Backup files have been transffered into temporary folder at $BACKUP_TEMP_PATH" | tee -a $REPORT_FILE
else
  echo "[sysbckp-script] No existing backup is available in $BACKUP_NEW_PATH" | tee -a $REPORT_FILE
fi

# Exit script
exit 0
}

##############
#Function call:

sysbckp_clean