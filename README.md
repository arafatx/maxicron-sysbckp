# Backup Script for directadmin by Arafat @ MaXi32

# Introduction

There is a feature in directadmin web hosting control panel call "Backup System" which allows us to backup files and several settings with defined custom path. I use this feature a lot.

In order to be able to view the backup files created by "Backup System" in directadmin file manager by specific user, (Not User/Admin Backup/Transfer), we need to be able to run a script and make this happen after the backup is completed

The current version fo directadmin (1.604000) does not include predefined template for "user_backup_post.sh" or "all_backup_post.sh" for "Backup System feature" it does work only for Admin Backup/Transfer

This script can be run as standalone, In order to make the script work together with the Backup System after it has completed making a system backup, 

Edit this file and call the backup script function at the comnment section #TODO:

nano /usr/local/sysbk/internals/internals.sysbk

# prebk()
```
prebk() {
head
echo -n "Performing sanity checks: "
...
...
...
if [ "$USE_RTRANS" = "1" ] && [ "$DEL_AFTERTRANS" = "1" ]; then
        rm -rf $BACKUP_PATH/$DATE
fi
echo " Completed" >> $QLOG
echo_completed
############ START TODO ###############
echo -n "Running backup script check: "
echo -n "Running backup script check: " >> $QLOG
/usr/local/bin/maxicron/sysbckp/sysbckp-post.sh 12345
echo " Completed: "
echo " Completed" >> $QLOG**
############ END TODO ################
}
```

# postbk()
```
postbk() {
echo -n "Performing cleanup operations: "
echo -n "Performing cleanup operations: " >> $QLOG
rm -f $TMP_FILE
rm -f $IGNORE
if [ ! -f "$MYSQL_PID" ]; then
        $MYSQL_INIT start >> /dev/null 2>&1
fi
if [ "$USE_RTRANS" = "1" ] && [ "$DEL_AFTERTRANS" = "1" ]; then
        rm -rf $BACKUP_PATH/$DATE
fi
echo " Completed" >> $QLOG
echo_completed
############ START TODO ###############
echo -n "Running backup script check: "
echo -n "Running backup script check: " >> $QLOG
/usr/local/bin/maxicron/sysbckp/sysbckp-post.sh 12345
echo " Completed: "
echo " Completed" >> $QLOG
############ END TODO ################
}
```

Related problem (thanks @ zEitEr for giving hint from the directadmin forum):

https://forum.directadmin.com/threads/system-backup-post-script-all_backup_post-sh.60466/#post-311622