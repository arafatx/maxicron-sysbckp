#!/bin/sh
# sysbckp-check.sh
# Author - Arafat Ali
# Kerja Perintah Berkurung #Covid19
# This script will only run once when directadmin system_backups is running. So, technically, only 1 file is supposed to be archived at the
# variable FOLDER_DIR_TO_ARCHIVE. For multiple folder backup use this:
#for FOLDER_DIR_TO_ARCHIVE in "${paths[@]}"
#do
#    echo "$path"
#done
#this is like hardcoding code, i will clean up this code in the future.
#############
script_name=$(basename -- "$0")
pid=(`pgrep -f $script_name`)
pid_count=${#pid[@]}

USER_NAME=admin #directadmin username. which user should view this backup file in file manager. example admin
BACKUP_SECURITY_CODE="12345"
BACKUP_TEMP_PATH=/backup/da/temp
BACKUP_ORI_PATH=/backup/da/system_backups
BACKUP_NEW_PATH=/home/$USER_NAME/system_backups
BACKUP_DATE_NOW="$(date '+%d-%m-%Y_%H-%M-%S-%p')" #31-03-2020--11-56-16-AM
BACKUP_FILE_NAME_NOW="system_backups-$BACKUP_DATE_NOW.tar.gz" #system_backups-31-03-2020--11-56-16-AM.tar.gz
BACKUP_ORI_FULL_PATH="$BACKUP_ORI_PATH/$BACKUP_FILE_NAME_NOW"
BACKUP_NEW_FULL_PATH="$BACKUP_NEW_PATH/$BACKUP_FILE_NAME_NOW"
BACKUP_DIRS=(/backup/da/system_backups/*) #MULTIPLE array
BACKUP_DIR_TO_ARCHIVE="${BACKUP_DIRS[0]}" #SINGLE /backup/da/system_backup/31-03-2020
MAIL_BIN="/usr/local/bin/mail"
WARNING_STATUS="OK"
MYEMAIL="webmaster@sofibox.com"
MYHOSTNAME=`/bin/hostname`
SYSBCKP_LOG_PATH="/usr/local/bin/maxicron/sysbckp/log"
REPORT_FILE="$SYSBCKP_LOG_PATH/sysbckp-cron.log"
RUNNING_ARGS="$1" #pass-backup-code
#TODO
#BACKUP_TYPE = zip / tar.gz / rar
#DELETE_ENTRY=0
#ADD_ENTRY=0
#CHANGE_ENTRY=0
# IMPORTANT TODO: DELETE LOG FILES WITHIN SPECIFIC DAY
#LOG_DAYS_TO_KEEP=0
#############
#echo "$USER $BACKUP_NEW_FULL_PATH: $USER $BACKUP_NEW_FULL_PATH"
echo "" | tee -a  $REPORT_FILE
echo "================================================================" | tee -a  $REPORT_FILE
echo "[sysbckp-script] System Backup Script checked on `date`" >> $REPORT_FILE
[[ -z $pid ]] && echo "Failed to get the PID"

if [ -f "/var/run/$script_name" ];then
   if [[  $pid_count -gt "1" ]];then
      WARNING_STATUS="WARNING"
      echo "=================================" | tee -a $REPORT_FILE
      echo "[sysbckp-script][error] Another instance of $script_name script is already running, clear all the sessions of $script_name to initialize session" | tee -a $REPORT_FILE
      echo "[sysbckp-script][error] for this reason, $script_name script is terminated" | tee -a $REPORT_FILE
      echo "===============================" | tee -a $REPORT_FILE
      $MAIL_BIN -s "[sysbckp-script][$WARNING_STATUS|N] System Backup Script Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
      echo "[sysbckp-script][done] Email is set to be sent to $MYEMAIL"
      echo "==============================="
      #Remove log and lock status
      rm -f $REPORT_FILE
      rm -f "/var/run/$script_name"
      # terminate script
      exit 1
   else
      echo "=================================" | tee -a $REPORT_FILE
      echo "[sysbckp-script][warning] The last instance of $script_name script exited unsuccessfully, performing cleanup..." | tee -a $REPORT_FILE
      rm -f "/var/run/$script_name"
      echo "[sysbckp-script][done] An instance of $script_name script has been removed successfully from the lock" | tee -a $REPORT_FILE
      echo "=================================" | tee -a $REPORT_FILE
   fi
fi

echo $pid > /var/run/$script_name

if [ "$RUNNING_ARGS" != "$BACKUP_SECURITY_CODE" ]; then
   WARNING_STATUS="WARNING"
   echo "[sysbckp-script][error] This backup script need to run with backup security code as the argument for security purpose" | tee -a $REPORT_FILE
   echo "[sysbckp-script][error] No backup security code or invalid backup security code is supplied. This script is terminated for this reason" | tee -a  $REPORT_FILE
   $MAIL_BIN -s "[sysbckp][$WARNING_STATUS|N] System Backup Script Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
      #Remove log and lock status
   rm -f $REPORT_FILE
   rm -f "/var/run/$script_name"
   # terminate script
   exit 1
fi
if ! find "$BACKUP_ORI_PATH" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
   WARNING_STATUS="WARNING"
   echo "[sysbckp-script][error] Backup directory to archive is empty or does not exist" | tee -a $REPORT_FILE
   echo "[sysbckp-script][error] This script is terminated for this reason" | tee -a $REPORT_FILE
   $MAIL_BIN -s "[sysbckp-script][$WARNING_STATUS|N] System Backup Script Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
   #Remove log and lock status
   rm -f $REPORT_FILE
   rm -f "/var/run/$script_name"
   # terminate script
   exit 1
fi
# Archive the single backup
#tar -zcvf "$BACKUP_ORI_FULL_PATH" "$BACKUP_DIR_TO_ARCHIVE" >> $REPORT_FILE
tar -c --use-compress-program=pigz -f "$BACKUP_ORI_FULL_PATH" "$BACKUP_DIR_TO_ARCHIVE" >> $REPORT_FILE

# Verify the backup file validiy
#if ! tar -xvzf "$BACKUP_ORI_FULL_PATH" -O &> /dev/null; then
#if gzip -t "$BACKUP_ORI_FULL_PATH"; then
if ! pigz -cvd "$BACKUP_ORI_FULL_PATH" | tar -tv > /dev/null; then
   echo "[sysbckp-script][error] File validation is not passed! Please inspect the current backup file" |tee -a $REPORT_FILE
   WARNING_STATUS="WARNING"
   $MAIL_BIN -s "[sysbckp-script][$WARNING_STATUS|N] System Backup Script Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
else
  echo "[sysbckp-script][ok] Backup file integrity passed!" | tee -a $REPORT_FILE
  #Delete the original backup folder that was used to archive
  rm -rf "$BACKUP_DIR_TO_ARCHIVE"
  #Move the complete backup file from ORIGINAL path to the NEW user path
  mv $BACKUP_ORI_FULL_PATH $BACKUP_NEW_FULL_PATH
  #Change the file permissions of the complete files which located at the New full path
  #echo "$USER $BACKUP_NEW_FULL_PATH: $USER $BACKUP_NEW_FULL_PATH"
  chown -R $USER_NAME:$USER_NAME $BACKUP_NEW_FULL_PATH
  chmod -R 644 $BACKUP_NEW_FULL_PATH
fi
# OTHER CHECK TO DO HERE AND IF WARNING STATUS = YES SEND EMAIL
echo "[sysbckp-script][done] Done running System Backup Script" | tee -a $REPORT_FILE
echo "================================================================" | tee -a  $REPORT_FILE
#Retransfer previous backup at the temporary file to the original folder
if [ "$(ls -A $BACKUP_TEMP_PATH)" ]; then
mv "${BACKUP_TEMP_PATH}"/* "${BACKUP_NEW_PATH}"/
echo "[sysbckp-script][done] Backup files have been transfered back into the original folder" | tee -a $REPORT_FILE
fi

#if [ "${WARNING_STATUS}" == "WARNING" ]; then
 $MAIL_BIN -s "[sysbckp-script][$WARNING_STATUS] System Backup Script Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
#fi

# Remove log and lock status
rm -f $REPORT_FILE
rm -f "/var/run/$script_name"